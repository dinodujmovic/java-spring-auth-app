package com.app.security.task;


import com.app.security.user.repository.UserJpaRepository;
import com.app.security.user.entity.VerificationToken;
import com.app.security.user.repository.VerificationTokenJpaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class TokensPurgeTask {

    @Autowired
    VerificationTokenJpaRepository tokenRepository;

    @Autowired
    UserJpaRepository userJpaRepository;

    @Scheduled(cron = "${global.scheduling.vTokenCron}")
    public void purgeExpired() {
        // Clean all tokens and users - everyday at 5:00 AM
        // if token date is older than today
        Date now = Date.from(Instant.now());
        List<VerificationToken> tokens = tokenRepository.findAllByExpiryDateLessThanNow();
        List<String> expiredUsers = new ArrayList<String>();
        for (VerificationToken expired : tokens) {
            expiredUsers.add(expired.getUser().getId());
        }
        tokenRepository.deleteAllExpiredSince(now);
        if (!expiredUsers.isEmpty()) {
            userJpaRepository.deleteByIdIn(expiredUsers);
        }
    }
}