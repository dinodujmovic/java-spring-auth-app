package com.app.security.registration.service;

import com.app.security.user.entity.User;
import com.app.security.user.entity.UserRole;
import com.app.security.user.entity.VerificationToken;
import com.app.security.registration.exception.UserAlreadyExistException;
import com.app.security.registration.exception.UserPasswordDoNotMatchException;
import com.app.security.registration.model.UserAccountDto;
import com.app.security.user.repository.UserJpaRepository;
import com.app.security.user.repository.UserRoleJpaRepository;
import com.app.security.user.repository.VerificationTokenJpaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Locale;

@Service
public class RegistrationService {

    @Autowired
    UserJpaRepository userJpaRepository;

    @Autowired
    UserRoleJpaRepository userRoleJpaRepository;

    @Autowired
    VerificationTokenJpaRepository tokenRepository;

    private BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    public User registerNewUserAccount(UserAccountDto newUser, Locale locale) {
        if (usernameOrEmailExists(newUser.getUsername(), newUser.getEmail())) {
            throw new UserAlreadyExistException("Username/email already exist !");
        }

        if (!newUser.getPassword().equals(newUser.getMatchingPassword())) {
            throw new UserPasswordDoNotMatchException("Passwords do not match !");
        }
        final User user = new User();

        user.setName(newUser.getName());
        user.setSurname(newUser.getSurname());
        user.setUsername(newUser.getUsername());
        user.setPassword(passwordEncoder.encode(newUser.getPassword()));
        user.setEmail(newUser.getEmail());
        user.setEnabled(false);

        return userJpaRepository.saveAndFlush(user);
    }

    public String validateVerificationToken(String token) {
        final VerificationToken verificationToken = tokenRepository.findByToken(token);
        if (verificationToken == null) {
            return "invalid";
        }

        final User user = verificationToken.getUser();
        final Calendar cal = Calendar.getInstance();
        if ((verificationToken.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {
            tokenRepository.delete(verificationToken);
            userJpaRepository.removeUnactivatedUser(verificationToken.getUser().getId());
            return "token_expired";
        }

        // Update user to enabled !
        user.setEnabled(true);
        userJpaRepository.save(user);
        // Add role to user
        UserRole userRole = new UserRole();
        userRole.setRole_id(2L);    // Role: Member
        userRole.setUser_id(user.getId());
        userRoleJpaRepository.save(userRole);
        // Delete verification token - as it is no longer needed
        tokenRepository.delete(verificationToken);
        return "valid";
    }

    public void createVerificationToken(User user, String token) {
        VerificationToken myToken = new VerificationToken(token, user);
        tokenRepository.save(myToken);
    }

    // ==
    // Private methods
    // ==
    private boolean usernameOrEmailExists(String username, String email) {
        User user = userJpaRepository.findByUsernameOrEmail(username, email);
        if (user != null) {
            return true;
        }
        return false;
    }

}
