package com.app.security;

import java.util.Arrays;
import java.util.List;

import com.app.security.authentication.RestAuthenticationEntryPoint;
import com.app.security.authentication.auth.ajax.AjaxAuthenticationProvider;
import com.app.security.authentication.auth.ajax.AjaxLoginProcessingFilter;
import com.app.security.authentication.auth.jwt.JwtAuthenticationProvider;
import com.app.security.authentication.auth.jwt.JwtTokenAuthenticationProcessingFilter;
import com.app.security.authentication.auth.jwt.SkipPathRequestMatcher;
import com.app.security.authentication.auth.jwt.extractor.TokenExtractor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.fasterxml.jackson.databind.ObjectMapper;

@Configuration
@EnableWebSecurity
// Need to enable @PreAuthorize
// http://websystique.com/spring-security/spring-security-4-method-security-using-preauthorize-postauthorize-secured-el/
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    public static final String JWT_TOKEN_HEADER_PARAM = "X-Authorization";
    public static final String TOKEN_BASED_AUTH_ENTRY_POINT = "/api/**";
    public static final String REGISTRATION_ENTRY_POINT = "/api/registration";
    public static final String REGISTRATION_CONFIRM_ENTRY_POINT = "/confirm-registration";
    public static final String FORM_BASED_LOGIN_ENTRY_POINT = "/api/auth/login";
    public static final String TOKEN_REFRESH_ENTRY_POINT = "/api/auth/token";
    // public static final String PROFILE_AVATAR = "/api/me/avatar";

    @Autowired
    private RestAuthenticationEntryPoint authenticationEntryPoint;

    @Autowired
    private AuthenticationSuccessHandler successHandler;

    @Autowired
    private AuthenticationFailureHandler failureHandler;

    @Autowired
    private AjaxAuthenticationProvider ajaxAuthenticationProvider;

    @Autowired
    private JwtAuthenticationProvider jwtAuthenticationProvider;

    @Autowired
    private TokenExtractor tokenExtractor;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private ObjectMapper objectMapper;

    protected AjaxLoginProcessingFilter buildAjaxLoginProcessingFilter() throws Exception {
        // Create new filter for "/api/auth/login"
        AjaxLoginProcessingFilter filter = new AjaxLoginProcessingFilter(FORM_BASED_LOGIN_ENTRY_POINT, successHandler, failureHandler, objectMapper);
        filter.setAuthenticationManager(this.authenticationManager);
        return filter;
    }

    protected JwtTokenAuthenticationProcessingFilter buildJwtTokenAuthenticationProcessingFilter() throws Exception {
        // Check all paths from "/api/**" but skip check for ["api/auth/login", "api/auth/token", "api/registration", "confirm-registration"]
        List<String> pathsToSkip = Arrays.asList(
                TOKEN_REFRESH_ENTRY_POINT,
                FORM_BASED_LOGIN_ENTRY_POINT,
                REGISTRATION_ENTRY_POINT,
                REGISTRATION_CONFIRM_ENTRY_POINT);
        // PROFILE_AVATAR);
        SkipPathRequestMatcher matcher = new SkipPathRequestMatcher(pathsToSkip, TOKEN_BASED_AUTH_ENTRY_POINT);
        JwtTokenAuthenticationProcessingFilter filter
                = new JwtTokenAuthenticationProcessingFilter(failureHandler, tokenExtractor, matcher);
        filter.setAuthenticationManager(this.authenticationManager);
        return filter;
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) {
        auth.authenticationProvider(ajaxAuthenticationProvider);
        auth.authenticationProvider(jwtAuthenticationProvider);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable() // We don't need CSRF for JWT based authentication
                .exceptionHandling()
                .authenticationEntryPoint(this.authenticationEntryPoint)

                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)

                .and()
                .authorizeRequests()
                .antMatchers(HttpMethod.GET, "/", "/*.ico", "/*.json", "/*.html", "/*.js", "/*.css", "/*.woff", "/*.woff2", "/*.ttf", "/assets/**", "/registration/**", "/files/**").permitAll()
                .antMatchers(HttpMethod.POST, FORM_BASED_LOGIN_ENTRY_POINT).permitAll() // Login end-point
                .antMatchers(HttpMethod.POST, REGISTRATION_ENTRY_POINT).permitAll() // Registration end-point
                .antMatchers(HttpMethod.GET, REGISTRATION_CONFIRM_ENTRY_POINT).permitAll() // Confirm registration end-point
                // .antMatchers(HttpMethod.GET, PROFILE_AVATAR).permitAll() // Get profile avatar (so it can be obtained with <img src="avatarPath?token">
                .antMatchers(TOKEN_REFRESH_ENTRY_POINT).permitAll() // Token refresh end-point
                .and()
                .authorizeRequests()
                // We can protect routes with roles - here (or we can use @PreAuthorize anotations
                //.antMatchers("/api/users").hasRole("ADMIN")
                .antMatchers(TOKEN_BASED_AUTH_ENTRY_POINT).authenticated() // Protected API End-points
                .and()
                .addFilterBefore(new CustomCorsFilter(), UsernamePasswordAuthenticationFilter.class)

                // Filter for /api/login
                // TODO: Don't understand this UsernamePasswordAuthenticationFilter.class
                .addFilterBefore(buildAjaxLoginProcessingFilter(), UsernamePasswordAuthenticationFilter.class)
                // Filter for every request that requires authentication
                .addFilterBefore(buildJwtTokenAuthenticationProcessingFilter(), UsernamePasswordAuthenticationFilter.class);
    }
}
