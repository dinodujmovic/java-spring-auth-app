package com.app.security.user.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@Entity
@Table(name = "users_roles")
public class UserRole {

    @Id
    @JsonIgnore
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    // --
    // Foreign keys
    // --
    @JsonIgnore
    @NotNull
    private String user_id;

    @JsonIgnore
    @NotNull
    private Long role_id;

    // --
    // Joins
    // --
    @ManyToOne
    @JoinColumn(name = "role_id", updatable = false, insertable = false)
    private Role role;


    // ====
    // Constructors
    // ====
    public UserRole() {
    }

    // ====
    // Getters and Setters
    // ====
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public Long getRole_id() {
        return role_id;
    }

    public void setRole_id(Long role_id) {
        this.role_id = role_id;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}
