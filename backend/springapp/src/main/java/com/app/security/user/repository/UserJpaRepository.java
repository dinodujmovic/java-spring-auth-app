package com.app.security.user.repository;

import java.util.List;
import java.util.Optional;

import com.app.security.user.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;

public interface UserJpaRepository extends JpaRepository<User, String> {

    public Optional<User> findById(@Param("id") String id);

    @Query("select u from User u left join fetch u.roles r where u.username=:username")
    public Optional<User> findByUsername(@Param("username") String username);

    @Query("select u from User u left join fetch u.roles r where u.username=:username and u.enabled = 1")
    public Optional<User> findByUsernameAndEnabled(@Param("username") String username);

    public User findByUsernameOrEmail(String username, String email);

    @Modifying
    @Transactional
    @Query("delete from User u where u.id IN (:ids)")
    public void deleteByIdIn(@Param(value = "ids") List<String> ids);

    @Modifying
    @Transactional
    @Query("delete from User u where u.id = :id")
    public void removeUnactivatedUser(@Param(value = "id") String id);
}
