package com.app.security.registration.endpoint;

import com.app.security.user.entity.User;
import com.app.security.registration.event.OnRegistrationCompleteEvent;
import com.app.security.registration.model.UserAccountDto;
import com.app.security.registration.service.RegistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.util.Locale;

@RestController
public class RegistrationEndpoint {

    @Autowired
    private RegistrationService registrationService;

    @Autowired
    ApplicationEventPublisher eventPublisher;

    @RequestMapping(value = "/api/registration", method = RequestMethod.POST)
    public UserAccountDto registerUserAccount(@RequestBody @Valid final UserAccountDto accountDto, final HttpServletRequest request, Locale locale) {
        final User registeredUser = registrationService.registerNewUserAccount(accountDto, locale);
        // After new user is registered (enabled = false) we want to send an event
        // we pass registered user (returned from db), locale (if needed), and application url/domain
        // event will be triggered on registration.event.RegistrationListener class that will create new VerificationToken and send email
        // for registration confirmation
        eventPublisher.publishEvent(new OnRegistrationCompleteEvent(registeredUser, request.getLocale(), getAppUrl(request)));
        return accountDto;
    }


    // ==
    //  returned by email
    //  www.mysite.com/confirm-registration?token=abcd1234567
    // ==
    @RequestMapping(value = "confirm-registration", method = RequestMethod.GET)
    public void confirmRegistration(HttpServletResponse response, @RequestParam("token") final String token) throws IOException {
        final String result = registrationService.validateVerificationToken(token);
        // if token is valid - send user to 'static' success-register.html
        if (result.equals("valid")) {
            response.sendRedirect("registration/success-register.html");
        } else {
            response.sendRedirect("registration/bad-user.html");
        }
    }

    // ==
    // Private methods
    // ==
    private String getAppUrl(HttpServletRequest request) {
        return "http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
    }
}
