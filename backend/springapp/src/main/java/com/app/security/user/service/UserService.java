package com.app.security.user.service;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import com.app.security.authentication.model.UserContext;
import com.app.security.user.entity.User;
import com.app.security.user.repository.UserJpaRepository;
import com.app.security.user.service.dto.GetUserDto;
import com.app.security.user.service.dto.mapper.UserMapper;
import com.app.utils.Helper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class UserService {

    @Autowired
    UserMapper userMapper;

    @Autowired
    UserJpaRepository userJpaRepository;

    public List<GetUserDto> getAllUsers() {
        List<User> users = userJpaRepository.findAll();
        if (users.isEmpty() || users == null) {
            return null;
        }
        return userMapper.getUserToUserDto(users);
    }

    public Optional<User> getUserById(String userId) {
        return this.userJpaRepository.findById(userId);
    }

    public byte[] getUserAvatar(String userId) {
        User existingUser = this.userJpaRepository.findById(userId)
                .orElseThrow(() ->
                        new RuntimeException("User doesn't exist!")
                );
        return existingUser.getAvatar();
    }

    public User updateUser(UserContext user, String name, String surname, MultipartFile avatar) throws IOException {
        User entryUser = new User();
        entryUser.setName(name);
        entryUser.setSurname(surname);
        if (avatar != null) {
            entryUser.setAvatar(avatar.getBytes());
        }
        entryUser.setEnabled(true);
        User existingUser = this.userJpaRepository.findById(user.getId())
                .orElseThrow(() ->
                        new RuntimeException("User doesn't exist!")
                );
        BeanUtils.copyProperties(entryUser, existingUser, Helper.getNullPropertyNames(entryUser));
        return userJpaRepository.save(existingUser);
    }

    public Optional<User> getByUsername(String username) {
        return this.userJpaRepository.findByUsername(username);
    }

    public Optional<User> getByUsernameAndEnabled(String username) {
        return this.userJpaRepository.findByUsernameAndEnabled(username);
    }
}
