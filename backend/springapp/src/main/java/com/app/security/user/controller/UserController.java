package com.app.security.user.controller;

import com.app.security.user.service.UserService;
import com.app.security.user.service.dto.GetUserDto;
import com.app.security.authentication.auth.JwtAuthenticationToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UserController {

    @Autowired
    UserService userService;

    /*
    * GET: Get all users
    * */

    // Don't mix authorities and roles (which are authorities with the "ROLE_" prefix),
    // use either hasRole('ADMIN') or hasAuthority('ROLE_ADMIN')
    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(value = "/api/users", method = RequestMethod.GET)
    public ResponseEntity<List<GetUserDto>> getAllUsers(JwtAuthenticationToken token) {
        List<GetUserDto> users = userService.getAllUsers();
        if (users.isEmpty() || users == null) {
            return new ResponseEntity<>(null, null, HttpStatus.NOT_MODIFIED);
        }
        return new ResponseEntity<>(users, null, HttpStatus.OK);
    }

    /*
    * GET: Get users avatar by userId
    * */
    @RequestMapping(value = "/api/users/{userId}/avatar", method = RequestMethod.GET)
    public ResponseEntity<byte[]> getUserAvatar(@PathVariable String userId,
                                                @RequestParam(value = "token") String token) {
        if (token == null || token.isEmpty()) {
            return new ResponseEntity<>(null, null, HttpStatus.NO_CONTENT);
        }

        byte[] userAvatar = userService.getUserAvatar(userId);
        if (userAvatar == null) {
            return new ResponseEntity<>(null, null, HttpStatus.NO_CONTENT);
        }
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.IMAGE_PNG);
        return new ResponseEntity<>(userAvatar, headers, HttpStatus.OK);
    }
}
