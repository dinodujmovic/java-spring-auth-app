package com.app.security.user.service.dto.mapper;

import com.app.security.user.service.dto.GetUserDto;
import com.app.security.user.entity.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public final class UserMapper {

    // --
    // GET
    // --
    public GetUserDto getUserToUserDto(final User user) {
        if (user == null) {
            return null;
        }

        List<GrantedAuthority> authorities = user.getRoles().stream()
                .map(authority -> new SimpleGrantedAuthority(authority.getRole().authority()))
                .collect(Collectors.toList());

        GetUserDto getUserDto = new GetUserDto(
                user.getId(),
                user.getUsername(),
                user.getName(),
                user.getSurname(),
                user.getEmail(),
                "/users/" + user.getId() + "/avatar/",
                user.isEnabled(),
                authorities);

        return getUserDto;
    }

    public List<GetUserDto> getUserToUserDto(final List<User> users) {
        List<GetUserDto> usersDto = new ArrayList<>();
        if (users == null && users.isEmpty()) {
            return null;
        }
        users.forEach((User user) -> {
            GetUserDto getUserDto = getUserToUserDto(user);
            usersDto.add(getUserDto);
        });
        return usersDto;
    }

    // --
    // UPDATE
    // --
    // setUserDtoToUser
}
