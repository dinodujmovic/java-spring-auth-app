package com.app.security.registration.exception;

public final class UserPasswordDoNotMatchException extends RuntimeException {

    public UserPasswordDoNotMatchException() {
        super();
    }

    public UserPasswordDoNotMatchException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public UserPasswordDoNotMatchException(final String message) {
        super(message);
    }

    public UserPasswordDoNotMatchException(final Throwable cause) {
        super(cause);
    }

}