package com.app.security.authentication.endpoint;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.ServletException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.app.security.authentication.model.UserDto;
import com.app.security.authentication.auth.jwt.extractor.TokenExtractor;
import com.app.security.authentication.auth.jwt.verifier.TokenVerifier;
import com.app.security.authentication.config.JwtSettings;
import com.app.security.WebSecurityConfig;
import com.app.security.authentication.exception.InvalidJwtToken;
import com.app.security.authentication.model.UserContext;
import com.app.security.user.service.UserService;
import com.app.security.user.entity.User;
import com.app.security.authentication.model.token.JwtToken;
import com.app.security.authentication.model.token.JwtTokenFactory;
import com.app.security.authentication.model.token.RawAccessJwtToken;
import com.app.security.authentication.model.token.RefreshToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;

@RestController
public class RefreshTokenEndpoint {
    @Autowired
    private JwtTokenFactory tokenFactory;
    @Autowired
    private JwtSettings jwtSettings;
    @Autowired
    private UserService userService;
    @Autowired
    private TokenVerifier tokenVerifier;
    @Autowired
    @Qualifier("jwtHeaderTokenExtractor")
    private TokenExtractor tokenExtractor;

    @RequestMapping(value = "/api/auth/token", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    public @ResponseBody
    JwtToken refreshToken(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String tokenPayload = tokenExtractor.extract(request.getHeader(WebSecurityConfig.JWT_TOKEN_HEADER_PARAM));
        RawAccessJwtToken rawToken = new RawAccessJwtToken(tokenPayload);
        RefreshToken refreshToken = RefreshToken.create(rawToken, jwtSettings.getTokenSigningKey()).orElseThrow(() -> new InvalidJwtToken());

        String jti = refreshToken.getJti();
        if (!tokenVerifier.verify(jti)) {
            throw new InvalidJwtToken();
        }

        String subject = refreshToken.getSubject();
        User user = userService.getByUsername(subject).orElseThrow(() -> new UsernameNotFoundException("User not found: " + subject));

        if (user.getRoles() == null) throw new InsufficientAuthenticationException("User has no roles assigned");
        List<GrantedAuthority> authorities = user.getRoles().stream()
                .map(authority -> new SimpleGrantedAuthority(authority.getRole().authority()))
                .collect(Collectors.toList());

        UserDto userDto = new UserDto(
                user.getId(),
                user.getUsername(),
                user.getName(),
                user.getSurname(),
                user.getEmail(),
                "me/avatar");  // path to avatar img
        UserContext userContext = UserContext.create(userDto, authorities);

        //JwtToken accessToken = tokenFactory.createAccessJwtToken(userContext);
        //JwtToken refreshToken = tokenFactory.createRefreshToken(userContext);

        //Map<String, Object> tokenMap = new HashMap<String, Object>();
        //tokenMap.put("user", userContext);
        //tokenMap.put("token", accessToken.getToken());
        //tokenMap.put("refreshToken", refreshToken.getToken());

        return tokenFactory.createAccessJwtToken(userContext);
    }
}
