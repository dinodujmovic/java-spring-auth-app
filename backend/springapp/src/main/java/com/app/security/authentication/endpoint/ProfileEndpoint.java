package com.app.security.authentication.endpoint;

import com.app.security.WebSecurityConfig;
import com.app.security.authentication.auth.JwtAuthenticationToken;
import com.app.security.authentication.auth.jwt.extractor.TokenExtractor;
import com.app.security.authentication.config.JwtSettings;
import com.app.security.authentication.model.UserContext;
import com.app.security.authentication.model.UserDto;
import com.app.security.user.service.UserService;
import com.app.security.user.entity.User;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class ProfileEndpoint {

    @Autowired
    UserService userService;

    @Autowired
    private JwtSettings jwtSettings;

    @Autowired
    @Qualifier("jwtHeaderTokenExtractor")
    private TokenExtractor tokenExtractor;

    /*
    * Method: GET
    * Get authenticated user information
    * Should be called every time client app refresh - so you can check that you are still logged in
    **/
    @RequestMapping(value = "/api/me", method = RequestMethod.GET)
    public @ResponseBody
    UserContext get(HttpServletRequest request, JwtAuthenticationToken token) {
        String tokenPayload = tokenExtractor.extract(request.getHeader(WebSecurityConfig.JWT_TOKEN_HEADER_PARAM));
        String userId = (String) Jwts.parser().setSigningKey(jwtSettings.getTokenSigningKey()).parseClaimsJws(tokenPayload).getBody().get("user_id");
        User user = userService.getUserById(userId).get();

        List<GrantedAuthority> authorities = user.getRoles().stream()
                .map(authority -> new SimpleGrantedAuthority(authority.getRole().authority()))
                .collect(Collectors.toList());

        UserDto userDto = new UserDto(
                user.getId(),
                user.getUsername(),
                user.getName(),
                user.getSurname(),
                user.getEmail(),
                "/me/avatar");  // path to avatar img
        UserContext userContext = UserContext.create(userDto, authorities);
        // return (UserContext) token.getPrincipal();
        return userContext;
    }

    /*
    * Method: PUT
    * Update user info "name, surname, avatar"
    **/
    @RequestMapping(value = "/api/me", method = RequestMethod.PUT, consumes = "multipart/form-data")
    ResponseEntity<Long> updateProfile(JwtAuthenticationToken token,
                                       @RequestParam(required = false) MultipartFile avatar,
                                       @RequestParam(required = false) String name,
                                       @RequestParam(required = false) String surname) throws IOException {
        User updatedUser = userService.updateUser((UserContext) token.getPrincipal(), name, surname, avatar);
        if (updatedUser == null) {
            return new ResponseEntity<Long>(0L, null, HttpStatus.NOT_MODIFIED);
        }
        return new ResponseEntity<Long>(1L, null, HttpStatus.OK);
    }

    /*
    * Method: GET
    * Get user avatar
    * URL: /api/me/avatar?token=jwtToken123
    **/
    @RequestMapping(value = "/api/me/avatar", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<byte[]> getAvatar(@RequestParam(value = "token") String token) {
        if (token == null || token.isEmpty()) {
            return new ResponseEntity<byte[]>(null, null, HttpStatus.NO_CONTENT);
        }

        // Parse token - signinKey is in application.json
        String userId = (String) Jwts.parser().setSigningKey(jwtSettings.getTokenSigningKey()).parseClaimsJws(token).getBody().get("user_id");

        // How to return byte image from database - Java Spring
        // https://stackoverflow.com/questions/26400994/how-to-display-image-from-mysql-database-using-spring-mvc
        // https://stackoverflow.com/questions/2644542/outputting-a-byte-arrain-into-the-src-attribute-of-an-img-tag
        // https://stackoverflow.com/questions/40557637/how-to-return-an-image-in-spring-boot-controller-and-serve-like-a-file-system

        // USED:
        // https://stackoverflow.com/questions/5690228/spring-mvc-how-to-return-image-in-responsebody
        byte[] userAvatar = userService.getUserAvatar(userId);
        // I could also encode the array with
        // Base64.getEncoder().encodeToString(userAvatar) and return Base64 string
        if (userAvatar == null) {
            return new ResponseEntity<byte[]>(null, null, HttpStatus.NO_CONTENT);
        }

        final HttpHeaders headers = new HttpHeaders();
        // If we want to return correct type of stored file/byte
        // We should make column in database "fileType" with correct file type name
        // For this example: we always return PNG
        headers.setContentType(MediaType.IMAGE_PNG);
        return new ResponseEntity<byte[]>(userAvatar, headers, HttpStatus.OK);
    }
}
