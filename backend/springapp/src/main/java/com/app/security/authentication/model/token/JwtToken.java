package com.app.security.authentication.model.token;

public interface JwtToken {
    String getToken();
}
