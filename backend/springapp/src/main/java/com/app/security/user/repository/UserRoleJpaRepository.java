package com.app.security.user.repository;

import com.app.security.user.entity.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface UserRoleJpaRepository extends JpaRepository<UserRole, Long> {
}
