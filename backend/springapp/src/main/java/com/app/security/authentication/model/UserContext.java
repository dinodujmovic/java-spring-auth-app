package com.app.security.authentication.model;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.GrantedAuthority;

public class UserContext {
    private final String id;
    private final String name;
    private final String surname;
    private final String username;
    private final String email;
    private String avatar;

    private final List<GrantedAuthority> authorities;

    private UserContext(UserDto user, List<GrantedAuthority> authorities) {
        this.id = user.getId();
        this.name = user.getName();
        this.surname = user.getSurname();
        this.username = user.getUsername();
        this.email = user.getEmail();
        this.avatar = user.getAvatar();
        this.authorities = authorities;
    }

    public static UserContext create(UserDto user, List<GrantedAuthority> authorities) {
        if (StringUtils.isBlank(user.getUsername()))
            throw new IllegalArgumentException("Username is blank: " + user.getUsername());
        return new UserContext(user, authorities);
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getEmail() {
        return email;
    }

    public String getAvatar() {
        return avatar;
    }

    public String getUsername() {
        return username;
    }

    public List<GrantedAuthority> getAuthorities() {
        return authorities;
    }
}
