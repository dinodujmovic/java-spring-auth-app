package com.app.security.user.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "roles")
public class Role {
    @Id
    @JsonIgnore
    private Long id;

    private String name;

    public String authority() {
        return "ROLE_" + this.name;
    }

    // ====
    // Constructors
    // ====
    public Role() {
    }

    public Role(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    // ====
    // Getters and Setters
    // ====
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}




