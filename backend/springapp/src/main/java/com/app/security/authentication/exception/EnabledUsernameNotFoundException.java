package com.app.security.authentication.exception;

public final class EnabledUsernameNotFoundException extends RuntimeException {

    public EnabledUsernameNotFoundException() {
        super();
    }

    public EnabledUsernameNotFoundException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public EnabledUsernameNotFoundException(final String message) {
        super(message);
    }

    public EnabledUsernameNotFoundException(final Throwable cause) {
        super(cause);
    }

}