package com.app.main;

import lombok.Data;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class HomeController {

    @Data class Animal {
        String imageUrl;
        String type;
        String name;
        String color;

        public Animal(String imageUrl, String type, String name, String color) {
            this.imageUrl = imageUrl;
            this.type = type;
            this.name = name;
            this.color = color;
        }
    }

    @RequestMapping(value = "/api/animals", method = RequestMethod.GET)
    @ResponseBody
    public List<Animal> getCars() {
        List<Animal> animals = new ArrayList<>();
        animals.add(new Animal("https://i0.wp.com/catnamescity.com/wp-content/uploads/2017/02/unique-boy-cat-names.jpg?resize=300%2C300", "cat", "Jessy", "grey"));
        animals.add(new Animal("http://img2.zergnet.com/731737_300.jpg", "dog", "Zo", "brown"));
        animals.add(new Animal("https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcT81IJ3jRHWTyK9aoau748R8uJLF5BWLzElvCqZQ4RjtMuYXfmJ", "dog", "Donna", "white"));
        animals.add(new Animal("http://3.bp.blogspot.com/-_aj5vy0QdK4/TkK3S0gzAaI/AAAAAAAAAUY/fNwRhJjF09o/s400/Tiya+pakhi-2.png", "bird", "Joe", "green"));
        animals.add(new Animal("http://purelyfacts.com/pics/items/animals/Bear.jpg", "bear", "Kobe", "black"));
        return animals;
    }
}
