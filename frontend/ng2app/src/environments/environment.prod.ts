export const environment = {
  production: true,
  api: 'https://js-spring-auth-seed.herokuapp.com/api'
};
