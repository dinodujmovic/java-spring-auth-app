import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService, LoginContext } from '../core/authentication/authentication.service';
import { Router } from '@angular/router';
import { ToastsManager } from 'ng2-toastr';
import { HttpResponse } from '@angular/common/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  loginFormSubmitted: boolean;

  constructor(private router: Router,
              private fb: FormBuilder,
              private authenticationService: AuthenticationService,
              private toastr: ToastsManager) {
  }

  ngOnInit() {
    this.authenticationService.logout();
    this.loginForm = this.fb.group({
      username: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required)
    });
  }

  submit(form: FormGroup) {
    this.loginFormSubmitted = true;
    if (form.invalid) {
      return;
    }

    let loginContext: LoginContext = {
      username: form.value.username,
      password: form.value.password
    };

    this.authenticationService.login(loginContext)
      .subscribe((data) => {
        this.router.navigate(['/home']);
      }, (errorResponse) => {
        this.toastr.error(errorResponse.error.message, 'Error!');
      });
  }

}
