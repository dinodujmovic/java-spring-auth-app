import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpRequest } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Router } from '@angular/router';

export interface AuthenticatedUser {
  id: string;
  name: string;
  surname: string;
  username: string;
  email: string;
  avatar: string;
  authorities: { authority: string }[]; // roles
  authenticated: boolean;
}

export interface Credentials {
  user: AuthenticatedUser;
  token: string;
  refreshToken: string;
}

export interface LoginContext {
  username: string;
  password: string;
}

export interface RegistrationContext {
  name: string;
  surname: string;
  username: string;
  email: string;
  password: string;
  matchingPassword: string;
}

@Injectable()
export class AuthenticationService {

  public authToken: string;
  public refreshToken: string;
  public authenticatedUser: AuthenticatedUser;

  cachedRequests: Array<HttpRequest<any>> = [];

  constructor(private http: HttpClient, private router: Router) {
    this.authToken = localStorage.getItem('auth_token');
    this.refreshToken = localStorage.getItem('refresh_token');
  }

  register(context: RegistrationContext) {
    return this.http.post(`${environment.api}/registration`, context);
  }

  login(context: LoginContext) {
    return this.http.post(`${environment.api}/auth/login`, context)
      .map((data: Credentials) => {
        this.authenticatedUser = data.user;
        this.authenticatedUser.authenticated = true;
        this.authToken = data.token;
        this.refreshToken = data.refreshToken;
        localStorage.setItem('auth_token', data.token);
        localStorage.setItem('refresh_token', data.refreshToken);
        return data;
      });
  }

  logout() {
    this.authenticatedUser = null;
    this.authToken = null;
    this.refreshToken = null;
    localStorage.removeItem('auth_token');
    localStorage.removeItem('refresh_token');
    this.router.navigate(['/login']);
  }

  getAuthenticatedUser() {
    return new Promise((resolve, reject) => {
      return this.http.get(`${environment.api}/me`)
        .toPromise()
        .then((data: AuthenticatedUser) => {
          this.authenticatedUser = data;
          this.authenticatedUser.authenticated = true;
          resolve(this.authenticatedUser);
        }, (error) => {
          this.logout();
          reject(error);
        });
    });
  }

  isAuthenticatedUserAdmin() {
    if (this.authenticatedUser == null) {
      return false;
    }
    let role = this.authenticatedUser.authorities.filter((a) => {
      return a.authority === 'ROLE_ADMIN';
    });
    if (role.length) {
      return true;
    }
    return false;
  }
}

