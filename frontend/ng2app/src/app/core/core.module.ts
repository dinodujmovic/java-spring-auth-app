import { NgModule, Optional, SkipSelf } from '@angular/core';
import { HttpInterceptorService } from './http/http-interceptor.service';
import { AuthenticationService } from './authentication/authentication.service';
import { AuthenticationGuard } from './authentication/authentication.guard';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { MainModule } from '../main/main.module';
import { LoginModule } from '../login/login.module';
import { RegistrationModule } from '../registration/registration.module';

@NgModule({
  imports: [],
  exports: [
    MainModule,
    LoginModule,
    RegistrationModule
  ],
  declarations: [],
  providers: [
    AuthenticationService,
    AuthenticationGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpInterceptorService,
      multi: true,
    }]
})
export class CoreModule {

  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    // Import guard
    if (parentModule) {
      throw new Error(`${parentModule} has already been loaded. Import Core module in the AppModule only.`);
    }
  }
}
