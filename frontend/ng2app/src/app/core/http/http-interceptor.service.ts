import { Injectable, Injector } from '@angular/core';
import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { AuthenticationService } from '../authentication/authentication.service';

@Injectable()
export class HttpInterceptorService implements HttpInterceptor {

  constructor(private inj: Injector) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // Why like this:
    // https://github.com/angular/angular/issues/18224#issuecomment-316957213
    const authenticationService = this.inj.get(AuthenticationService);
    const dupReq = req.clone({
      setHeaders: {
        'X-Authorization': 'Bearer ' + authenticationService.authToken,
        'X-Requested-With': 'XMLHttpRequest'
      }
    });

    // https://medium.com/@ryanchenkie_40935/angular-authentication-using-the-http-client-and-http-interceptors-2f9d1540eb8
    return next.handle(dupReq).do((event: HttpEvent<any>) => {
      if (event instanceof HttpResponse) {
        // do stuff with response if you want
      }
    }, (err: any) => {
      if (err instanceof HttpErrorResponse) {
        // errorCode is defined by my API
        if (err.error.errorCode === 11) {
          // Refresh token and other !
          alert('Token expired');
          authenticationService.logout();
        }
      }
    });
  }

}
