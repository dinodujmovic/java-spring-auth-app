import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../core/authentication/authentication.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TextValidator } from '../shared/validators/text.validator';
import { ToastsManager } from 'ng2-toastr';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {


  registrationForm: FormGroup;
  registerFormSubmitted = false;
  registrationInProgress = false;

  constructor(private fb: FormBuilder,
              private authenticationService: AuthenticationService,
              private toastr: ToastsManager) {
  }

  ngOnInit() {
    this.registrationForm = this.fb.group({
      name: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(20)]],
      surname: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(20)]],
      email: ['', [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+')]],
      username: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(10)]],
      passwordGroup: this.fb.group({
        password: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(20)]],
        matchingPassword: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(20)]]
      }, {validator: TextValidator.passwordMatcher()})
    });
  }

  register(form) {
    this.registerFormSubmitted = true;
    if (form.invalid || this.registrationInProgress) {
      return;
    }
    this.registrationInProgress = true;

    let account = {
      name: form.value.name,
      surname: form.value.surname,
      username: form.value.username,
      email: form.value.email,
      password: form.value.passwordGroup.password,
      matchingPassword: form.value.passwordGroup.matchingPassword
    };
    console.log(account);

    this.authenticationService.register(account)
      .subscribe(response => {
          this.registrationInProgress = false;
          this.registerFormSubmitted = false;
          this.registrationForm.reset();
          this.toastr.success('Plase confirm registration by email', 'Success!');
        },
        errorResponse => {
          setTimeout(() => {
            this.registrationInProgress = false;
            this.toastr.error(errorResponse.error.message, 'Error!');
          }, 1500);
        });
  }

}
