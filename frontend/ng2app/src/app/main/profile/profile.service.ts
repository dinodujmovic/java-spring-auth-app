import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable()
export class ProfileService {

  constructor(private http: HttpClient) {
  }

  updateProfile(data: any) {
    let formData = new FormData();
    formData.append('name', data.name);
    formData.append('surname', data.surname);
    formData.append('avatar', data.avatar);
    console.log(data);
    return this.http.put(`${environment.api}/me`, formData);
  }
}
