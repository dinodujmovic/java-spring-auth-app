import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AuthenticationService } from '../../core/authentication/authentication.service';

@Injectable()
export class ProfileGuard implements CanActivate {
  constructor(private authenticatedService: AuthenticationService) {
  }

  canActivate(next: ActivatedRouteSnapshot,
              state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.authenticatedService.isAuthenticatedUserAdmin();
  }
}
