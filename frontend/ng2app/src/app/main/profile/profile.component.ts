import { Component, OnInit } from '@angular/core';
import { AuthenticatedUser, AuthenticationService } from '../../core/authentication/authentication.service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { environment } from '../../../environments/environment';
import { ProfileService } from './profile.service';
import { FileUploader } from 'ng2-file-upload';
import { DomSanitizer } from '@angular/platform-browser';
import * as _ from 'lodash';
import { ToastsManager } from 'ng2-toastr';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  profileForm: FormGroup;
  authenticatedUser: AuthenticatedUser;
  defaultAvatar: any;
  avatarFile: any;
  public avatarFileUploader: FileUploader = new FileUploader({disableMultipart: true, allowedFileType: ['image']});

  constructor(public sanitizer: DomSanitizer,
              private fb: FormBuilder,
              private authenticationService: AuthenticationService,
              private profileService: ProfileService,
              private toastr: ToastsManager) {
  }

  ngOnInit() {
    this.authenticatedUser = _.clone(this.authenticationService.authenticatedUser, true);
    this.authenticatedUser.avatar = `${environment.api}${this.authenticatedUser.avatar}?token=${this.authenticationService.authToken}`;
    this.defaultAvatar = _.clone(this.authenticatedUser.avatar, true);

    this.profileForm = this.fb.group({
      name: new FormControl(this.authenticatedUser.name, Validators.required),
      surname: new FormControl(this.authenticatedUser.surname, Validators.required)
    });
  }

  onAvatarChange(event: any) {
    let file = event.target.files[0];
    this.avatarFile = file;
    let url = (window.URL) ? window.URL.createObjectURL(file) : (window as any).webkitURL.createObjectURL(file);
    this.authenticatedUser.avatar = url;
    console.log(event.target.files[0]);
  }

  cancelAvatarChange() {
    this.avatarFile = null;
    this.avatarFileUploader.clearQueue();
    this.authenticatedUser.avatar = this.defaultAvatar;
  }

  update(form) {
    if (form.invalid) {
      return;
    }
    let dataForUpdate = {
      name: form.value.name,
      surname: form.value.surname,
      avatar: this.avatarFile
    };
    this.profileService.updateProfile(dataForUpdate)
      .subscribe((response: any) => {
        this.avatarFileUploader.clearQueue();
        this.authenticationService.getAuthenticatedUser()
          .then((data => {
            this.toastr.success('Profile updated!', 'Success!');
          }));
      }, (errorResponse: any) => {
        this.toastr.error(errorResponse.error.message, 'Error!');
      });
  }
}
