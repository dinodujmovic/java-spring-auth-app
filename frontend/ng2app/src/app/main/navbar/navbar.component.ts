import { Component, OnInit } from '@angular/core';
import { AuthenticatedUser, AuthenticationService } from '../../core/authentication/authentication.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  constructor(public authenticatedService: AuthenticationService) {
  }

  ngOnInit() {
  }

}
