import { NgModule } from '@angular/core';

import { MainRoutingModule } from './main-routing.module';
import { MainComponent } from './main.component';
import { SharedModule } from '../shared/shared.module';
import { ProfileComponent } from './profile/profile.component';
import { NavbarComponent } from './navbar/navbar.component';
import { HomeComponent } from './home/home.component';
import { UsersComponent } from './users/users.component';
import { UsersService } from './users/users.service';
import { ProfileGuard } from './profile/profile.guard';
import { HomeService } from './home/home.service';
import { ProfileService } from './profile/profile.service';

@NgModule({
  imports: [
    MainRoutingModule,
    SharedModule
  ],
  declarations: [
    MainComponent,
    ProfileComponent,
    NavbarComponent,
    HomeComponent,
    UsersComponent],
  providers: [
    HomeService,
    UsersService,
    ProfileService,
    ProfileGuard]
})
export class MainModule {
}
