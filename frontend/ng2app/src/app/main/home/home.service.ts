import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable()
export class HomeService {

  constructor(private http: HttpClient) {
  }

  getAnimals() {
    return this.http.get(`${environment.api}/animals`);
  }
}
