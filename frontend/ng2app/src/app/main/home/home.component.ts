import { Component, OnInit } from '@angular/core';
import { HomeService } from './home.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  animals: any;

  constructor(private homeService: HomeService) {
  }

  ngOnInit() {
    this.homeService.getAnimals()
      .subscribe((data: any) => {
        this.animals = data;
      });
  }

}
