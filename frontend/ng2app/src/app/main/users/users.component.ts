import { Component, OnInit } from '@angular/core';
import { UsersService } from './users.service';
import * as _ from 'lodash';
import { AuthenticationService } from '../../core/authentication/authentication.service';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  users: any;

  constructor(private usersService: UsersService,
              private authenticationService: AuthenticationService) {
  }

  ngOnInit() {
    this.usersService.getUsers()
      .subscribe((response: any) => {
        let users = response;
        _.each(users, (user: any) => {
          user.avatar = `${environment.api}${user.avatar}?token=${this.authenticationService.authToken}`;
        });
        this.users = _.sortBy(users, ['name']);
      });
  }

}
